# Goof Troop #

## Summary

This is not a ongoing fan-game! It's a vertical slice for the game Goofy Troop (SNES), also affectionately known in Brazil as **Pateta & Max**.

You can play the WebGL build [here](https://gamejolt.com/games/goof-troop/562684).

![alt text][thumbnail]

## Context

Project made to learn how to use 2D technologies, Timeline and UI animations.

![alt text][showcase]

### Spites

All Sprites from the Beach level were assembled as tiles with collisions. 

Some of them are animated using **Animated Tiles** and all of them use a **Sprite Atlas** for optimization.

### Physics

The player can move and collide using custom Physics components. It uses raycasts to detect collision. 

Also, there is a "slippery" collision which moves the player automatically when it collides with a solid object corner. 

## Copyrights

Goof and Troop is a 1993 game developed by Capcom.

**All rights reserved to Capcom and Disney Interactive Studios**.

---

**Hyago Oliveira**

<hyagogow@gmail.com>

[showcase]: /Documentation/showcase.gif "Showcase"
[thumbnail]: /Documentation/gamejolt/thumbnail.gif "Thumbnail"