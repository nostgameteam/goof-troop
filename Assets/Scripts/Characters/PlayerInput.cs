﻿using UnityEngine;
using UnityEngine.InputSystem;
using ActionCode.InputSystem;
using GoofTroop.UI;

namespace GoofTroop.Characters
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(CharacterController))]
    public sealed class PlayerInput : MonoBehaviour
    {
        [SerializeField] private InputActionAsset inputAction;
        [SerializeField] private CharacterController character;

        [Header("Action Names")]
        [SerializeField]
        private InputActionPopup moveAction = new InputActionPopup("inputAction", "Player", "Move");
        [SerializeField]
        private InputActionPopup useToolAction = new InputActionPopup("inputAction", "Player", "UseTool");
        [SerializeField]
        private InputActionPopup interactAction = new InputActionPopup("inputAction", "Player", "Interact");
        [SerializeField]
        private InputActionPopup pauseAction = new InputActionPopup("inputAction", "Player", "Pause");

        private InputAction moveInputAction;

        private void Reset()
        {
            character = GetComponent<CharacterController>();
        }

        private void Awake()
        {
            BindActions();
            moveInputAction = inputAction.FindAction(moveAction.actionName);
        }

        private void OnEnable()
        {
            inputAction.Enable();
        }

        private void Update()
        {
            MoveCharacter();
        }

        private void OnDisable()
        {
            inputAction.Disable();
        }

        private void BindActions()
        {
            var useTool = inputAction.FindAction(useToolAction.actionName);
            var interact = inputAction.FindAction(interactAction.actionName);
            var pause = inputAction.FindAction(pauseAction.actionName);

            var hasUseToolAction = useTool != null;
            var hasInteractAction = interact != null;
            var hasPauseAction = pause != null;

            if (hasUseToolAction) useTool.performed += _ => character.UseTool();
            if (hasInteractAction) interact.started += _ => character.Interact();
            if (hasPauseAction)
            {
                var pauseMenu = FindObjectOfType<PauseMenu>();
                //if (pauseMenu) pause.performed += _ => pauseMenu.Toggle();
            }
        }

        private void MoveCharacter()
        {
            var input = moveInputAction.ReadValue<Vector2>();
            character.Move(input);
        }
    }
}
