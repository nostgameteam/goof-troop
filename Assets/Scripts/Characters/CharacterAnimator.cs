using UnityEngine;

namespace GoofTroop.Characters
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Animator))]
    public sealed class CharacterAnimator : MonoBehaviour
    {
        [SerializeField]
        private Animator animator;

        private const string NORMAL_LAYER_NAME = "Normal";
        private const string RAISED_LAYER_NAME = "Raised";

        private readonly int IS_MOVING = Animator.StringToHash("isMoving");
        private readonly int VERTICAL_DIRECTION = Animator.StringToHash("verticalDirection");
        private readonly int HORIZONTAL_DIRECTION = Animator.StringToHash("horizontalDirection");

        private int normalLayerIndex;
        private int raisedLayerIndex;

        private void Reset()
        {
            animator = GetComponent<Animator>();
        }

        private void Awake()
        {
            LoadLayersIndex();
        }

        public void Move(Vector2 direction, bool isMoving)
        {
            animator.SetBool(IS_MOVING, isMoving);
            animator.SetFloat(VERTICAL_DIRECTION, direction.y);
            animator.SetFloat(HORIZONTAL_DIRECTION, direction.x);
        }

        public void LowerHands()
        {
            animator.SetLayerWeight(normalLayerIndex, 1F);
            animator.SetLayerWeight(raisedLayerIndex, 0F);
        }

        public void RaiseHands()
        {
            animator.SetLayerWeight(normalLayerIndex, 0F);
            animator.SetLayerWeight(raisedLayerIndex, 1F);
        }

        public bool IsHandsRaised()
        {
            return animator.GetLayerWeight(raisedLayerIndex) > 0F;
        }

        private void LoadLayersIndex()
        {
            normalLayerIndex = animator.GetLayerIndex(NORMAL_LAYER_NAME);
            raisedLayerIndex = animator.GetLayerIndex(RAISED_LAYER_NAME);
        }
    }
}