using UnityEngine;

namespace GoofTroop.Characters
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(CharacterMotor))]
    [RequireComponent(typeof(CharacterAnimator))]
    public sealed class CharacterController : MonoBehaviour
    {
        [SerializeField] private CharacterMotor motor;
        [SerializeField] private CharacterAnimator animator;

        private void Reset()
        {
            motor = GetComponent<CharacterMotor>();
            animator = GetComponent<CharacterAnimator>();
        }

        public void Move(Vector2 direction)
        {
            var absoluteMagnitude = Mathf.Abs(direction.sqrMagnitude);
            var isMoving = absoluteMagnitude > 0F;

            motor.Move(direction, isMoving);
            animator.Move(motor.FacingDirection, isMoving);
        }

        public void Interact()
        {
            if (animator.IsHandsRaised()) animator.LowerHands();
            else animator.RaiseHands();
        }

        public void UseTool()
        {

        }
    }
}