﻿using UnityEngine;
using ActionCode.BoxBody;

namespace GoofTroop.Characters
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(BoxBody))]
    public sealed class CharacterMotor : MonoBehaviour
    {
        [SerializeField] private BoxBody boxBody;
        public float speed = 12f;

        public Vector2 FacingDirection { get; private set; } = Vector2.down;

        private void Reset()
        {
            boxBody = GetComponent<BoxBody>();
        }

        public void Move(Vector2 direction, bool isMoving)
        {
            direction += GetSlipDirection(direction);
            boxBody.Move(direction.normalized, speed);
            if (isMoving) FacingDirection = direction;
        }

        private Vector2 GetSlipDirection(Vector2 movingDirection)
        {
            const int UP = 0;
            const int LEFT = 0;
            const int RIGHT = 2;
            const int DOWN = 2;
            const float INPUT_THRESHOLD = 0.8F;

            var isMovingUp = movingDirection.y > INPUT_THRESHOLD;
            var isMovingLeft = movingDirection.x < -INPUT_THRESHOLD;
            var isMovingRight = movingDirection.x > INPUT_THRESHOLD;
            var isMovingDown = movingDirection.y < -INPUT_THRESHOLD;

            var sliptToUpLeft = isMovingLeft &&
                !boxBody.LeftHit.IsHitColliding(UP) &&
                boxBody.LeftHit.IsHitColliding(DOWN);
            var sliptToDownLeft = isMovingLeft &&
                boxBody.LeftHit.IsHitColliding(UP) &&
                !boxBody.LeftHit.IsHitColliding(DOWN);

            var slipToLeftUp = isMovingUp &&
                !boxBody.TopHit.IsHitColliding(LEFT) &&
                boxBody.TopHit.IsHitColliding(RIGHT);
            var slipToLeftDown = isMovingDown &&
                !boxBody.BottomHit.IsHitColliding(LEFT) &&
                boxBody.BottomHit.IsHitColliding(RIGHT);

            var slipToRightUp = isMovingUp &&
                boxBody.TopHit.IsHitColliding(LEFT) &&
                !boxBody.TopHit.IsHitColliding(RIGHT);
            var slipToRightDown = isMovingDown &&
                boxBody.BottomHit.IsHitColliding(LEFT) &&
                !boxBody.BottomHit.IsHitColliding(RIGHT);

            var sliptToUpRight = isMovingRight &&
                !boxBody.RightHit.IsHitColliding(UP) &&
                boxBody.RightHit.IsHitColliding(DOWN);
            var sliptToDownRight = isMovingRight &&
                boxBody.RightHit.IsHitColliding(UP) &&
                !boxBody.RightHit.IsHitColliding(DOWN);

            var sliptToRight = slipToRightUp || slipToRightDown;
            var sliptToLeft = slipToLeftUp || slipToLeftDown;
            var sliptToUp = sliptToUpLeft || sliptToUpRight;
            var sliptToDown = sliptToDownLeft || sliptToDownRight;

            var direction = Vector2.zero;
            if (sliptToRight) direction.x = 1F;
            else if (sliptToLeft) direction.x = -1F;
            if (sliptToUp) direction.y = 1F;
            else if (sliptToDown) direction.y = -1F;

            return direction;
        }
    }
}
