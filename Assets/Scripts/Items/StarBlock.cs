﻿using UnityEngine;
using ActionCode.BoxBody;

namespace GoofTroop.Items
{
    [RequireComponent(typeof(BoxBody))]
    [RequireComponent(typeof(AudioSource))]
    public class StarBlock : MonoBehaviour, IKickable
    {
        [SerializeField] private BoxBody box;
        [SerializeField] private AudioSource audioSource;
        [SerializeField, Min(0F)]
        private float speed = 10F;

        private void Reset()
        {
            box = GetComponent<BoxBody>();
            audioSource = GetComponent<AudioSource>();
        }

        private void OnEnable()
        {
            box.OnHitAnySide += PlaySound;
        }

        private void OnDisable()
        {
            box.OnHitAnySide -= PlaySound;
        }

        public void Kick(Vector3 position)
        {
            var direction = (position - transform.position).normalized;
            box.Move(direction, speed);
        }

        private void PlaySound()
        {
            audioSource.Play();
        }

        [ContextMenu("KickUp")]
        private void KickUp()
        {
            box.Move(Vector2.up, speed);
        }

        [ContextMenu("KickRight")]
        private void KickRight()
        {
            box.Move(Vector2.right, speed);
        }

        [ContextMenu("KickLeft")]
        private void KickLeft()
        {
            box.Move(Vector2.left, speed);
        }

        [ContextMenu("KickDown")]
        private void KickDown()
        {
            box.Move(Vector2.down, speed);
        }
    }
}
