using UnityEngine;

namespace GoofTroop.Items
{
    /// <summary>
    /// Interface used on Objects able to be kicked.
    /// </summary>
    public interface IKickable
    {
        /// <summary>
        /// Kicks this object from the given position.
        /// </summary>
        /// <param name="position">Kicker position</param>
    	void Kick(Vector3 position);
    }
}