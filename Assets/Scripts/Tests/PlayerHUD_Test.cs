using UnityEngine;
using GoofTroop.UI;

namespace GoofTroop.Tests
{
    public sealed class PlayerHUD_Test : MonoBehaviour
    {
        public PlayerHUD hud;

        public int lives = 2;
        public int hearts = 6;

        [ContextMenu("Set Lives")]
        public void SetLives()
        {
            hud.Lives.Count = lives;
        }

        [ContextMenu("Set Hearts")]
        public void SetHearts()
        {
            hud.Hearts.Count = hearts;
        }
    }
}