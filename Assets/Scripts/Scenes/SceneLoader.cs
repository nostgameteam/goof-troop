using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Threading.Tasks;

namespace GoofTroop.Scenes
{
    public sealed class SceneLoader : MonoBehaviour
    {
        public void Load(string scene)
        {
            SceneManager.LoadScene(scene);
        }

        public async void Load(string scene, float delay)
        {
            await Task.Delay(TimeSpan.FromSeconds(delay));
            Load(scene);
        }
    }
}