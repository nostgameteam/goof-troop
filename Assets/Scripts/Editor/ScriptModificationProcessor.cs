﻿using System.IO;
using UnityEditor;
using UnityEditor.PackageManager;

namespace GoofTroop.Editor
{
    /// <summary>
    /// Modification processor to Script files.
    /// <para>Add the Project Settings > Editor > Root namespace to a new created script.</para>
    /// </summary>
    public sealed class ScriptModificationProcessor : UnityEditor.AssetModificationProcessor
    {
        public static void OnWillCreateAsset(string metaFilePath)
        {
            const string SCRIPT_EXTENSION = ".cs";

            var fileName = Path.GetFileNameWithoutExtension(metaFilePath);
            var invalidScriptFile = !fileName.EndsWith(SCRIPT_EXTENSION);
            if (invalidScriptFile) return;

            var directoryPath = Path.GetDirectoryName(metaFilePath);
            var filePath = $"{directoryPath}\\{fileName}";
            var content = File.ReadAllText(filePath);

            ReplaceNamespace(ref content, directoryPath);

            File.WriteAllText(filePath, content);
            AssetDatabase.Refresh();
        }

        private static void ReplaceNamespace(ref string content, string directoryPath)
        {
            const string NAMESPACE_KEY = "#NAMESPACE#";

            var _namespace = GetNamespace(directoryPath);
            content = content.Replace(NAMESPACE_KEY, _namespace);
        }

        private static string GetNamespace(string directoryPath)
        {
            var info = new DirectoryInfo(directoryPath);
            var directoryName = info.Name;
            var _namespace = EditorSettings.projectGenerationRootNamespace ?? PlayerSettings.productName;
            var validDirectory =
                !directoryName.Equals("Assets") &&
                !directoryName.Equals("Scripts") &&
                !directoryName.Equals("Runtime");

            if (validDirectory) _namespace += "." + directoryName;

            return _namespace;
        }
    }
}