using UnityEngine;
using UnityEngine.UI;

namespace GoofTroop.UI
{
    public sealed class HeartHUD : MonoBehaviour
    {
        [SerializeField] private Image firstRow;
        [SerializeField] private Image secondRow;

        public int Count
        {
            get { return count; }
            set
            {
                var isSameValue = count == value;
                if (isSameValue) return;

                count = Mathf.Clamp(value, MIN, MAX);

                var firstRowColumns = Mathf.Clamp(count, MIN, COLUMNS);
                var secondRowColumns = count > COLUMNS ? count - COLUMNS : 0;

                SetHeartsCount(firstRow, firstRowColumns);
                SetHeartsCount(secondRow, secondRowColumns);
            }
        }

        public const int MIN = 0;
        public const int MAX = 6;
        public const int PIXELS_PER_UNIT_SIZE = 8;
        public const int COLUMNS = 3;

        private int count = 6;

        private void SetHeartsCount(Image image, int count)
        {
            var width = count * PIXELS_PER_UNIT_SIZE;
            if (image)
            {
                image.rectTransform.sizeDelta = new Vector2(width, PIXELS_PER_UNIT_SIZE);
            }
        }
    }
}