using UnityEngine;
using UnityEngine.UI;

namespace GoofTroop.UI
{
    [RequireComponent(typeof(Canvas))]
    public sealed class PlayerHUD : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;
        [SerializeField] private Image portrait;
        [SerializeField] private LifeHUD lives;
        [SerializeField] private Image specialTool;
        [SerializeField] private HeartHUD hearts;

        public Sprite Portrait
        {
            get { return portrait.sprite; }
            set { portrait.sprite = value; }
        }

        public LifeHUD Lives => lives;

        public HeartHUD Hearts => hearts;

        private void Reset()
        {
            canvas = GetComponent<Canvas>();
            lives = GetComponentInChildren<LifeHUD>();
        }

        private void OnEnable()
        {
            canvas.enabled = true;
        }

        private void OnDisable()
        {
            canvas.enabled = false;
        }
    }
}