using UnityEngine;
using UnityEngine.SceneManagement;

namespace GoofTroop.UI
{
    public sealed class PauseMenu : MonoBehaviour
    {
        public void GoToMenu()
        {
            //Time.timeScale = 1f;
            SceneManager.LoadScene("MainMenu");
        }

        public void ResetGame()
        {
            //Time.timeScale = 1f;
            SceneManager.LoadScene(0);
        }
    }
}