using UnityEngine;
using UnityEngine.UI;

namespace GoofTroop.UI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Text))]
    public sealed class LifeHUD : MonoBehaviour
    {
        [SerializeField] private Text lives;

        public int Count
        {
            get { return count; }
            set
            {
                var isSameValue = count == value;
                if (isSameValue) return;

                count = Mathf.Clamp(value, MIN, MAX);
                lives.text = $"x{count}";
            }
        }

        public const int MIN = 0;
        public const int MAX = 99;

        private int count = 0;

        private void Reset()
        {
            lives = GetComponent<Text>();
        }
    }
}