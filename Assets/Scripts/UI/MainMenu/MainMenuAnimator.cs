using UnityEngine;

namespace GoofTroop.UI
{
    [RequireComponent(typeof(Animator))]
    public sealed class MainMenuAnimator : MonoBehaviour
    {
        [SerializeField] private Animator animator;

        private readonly int EXIT_TRIGGER = Animator.StringToHash("exit");

        private void Reset()
        {
            animator = GetComponent<Animator>();
        }

        public void SetExitTrigger()
        {
            animator.SetTrigger(EXIT_TRIGGER);
        }
    }
}