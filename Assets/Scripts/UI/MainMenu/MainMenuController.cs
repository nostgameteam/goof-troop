using UnityEngine;
using GoofTroop.Scenes;
using System.Collections;

namespace GoofTroop.UI
{
    [RequireComponent(typeof(MainMenuAnimator))]
    public sealed class MainMenuController : MonoBehaviour
    {
        [SerializeField] private SceneLoader sceneLoader;
        [SerializeField] private MainMenuAnimator animator;

        public string levelName = "Level-01";

        private void Reset()
        {
            sceneLoader = GetComponent<SceneLoader>();
            animator = GetComponent<MainMenuAnimator>();
        }

        public void GoToGame()
        {
            StartCoroutine(GoToGameCoroutine());
        }

        public IEnumerator GoToGameCoroutine()
        {
            yield return new WaitForSeconds(.5F);
            animator.SetExitTrigger();

            yield return new WaitForSeconds(1F);
            sceneLoader.Load(levelName);
        }
    }
}